<?php include('./inc/header-interna.php'); ?>

<?php include('./inc/timeline.php'); ?>

<div class="conteudo-interno">
	<?php include('./inc/breadcrumb.php'); ?>
	<div class="topo-interna">
		<img src="./images/topo-interna-por-dentro-do-tratamento.png" alt="">
		<h2>Por dentro do<br>tratamento</h2>
	</div>

		<div class="wrap-content tratamento">
		<div class="wrap-abas">
			<ul class="tabs">
				<li>
					<a id="sus" class="tab txt-normal" href="javascript:void(0);">O papel do SUS</a>
				</li>
				<li>
					<a id="direitos" class="tab txt-normal" href="javascript:void(0);">Evolução do tratamento nos últimos 30 anos</a>
				</li>
				<li>
					<a id="deu-positivo" class="tab txt-normal ativo" href="javascript:void(0);">Avanços <br>previstos</a>
				</li>
			</ul>
			<div class="tabs-content-container">
				<div id="sus" class="tab-content">
					<p class="marginBottomP">O Brasil é referência mundial no tratamento as pessoas que vivem com HIV/AIDSS, com uma política de distribuição gratuita e universal dos medicamentos antirretrovirais, que são aqueles que inibem   a multiplicação do vírus.</p>
					<p>O Sistema Único de Saúde garante a todas as pessoas que vivem com HIV/AIDS o direito ao diagnóstico, prevenção e tratamento. É graças ao SUS que centenas de milhares de brasileiros têm melhor qualidade de vida. Ao todo são 22 medicamentos antirretrovirais distribuídos em todo o Brasil nas unidades de saúde do SUS.</p>
				</div>
				<div id="direitos" class="tab-content">
					<p class="marginBottomP">Foi somente no começo da década de 80, logo após a identificação dos primeiros casos da doença, que se descobriu a origem viral da Aids, sendo um marco para o início das buscas por tratamento e aplicação de ações de prevenção.</p>

					<p class="marginBottomP">Com a descoberta criação do primeiro teste de HIV em 1985, foi possível realizar o diagnóstico . Porém, somente em 1987 passou a ser utilizado o AZT, medicamento inicialmente desenvolvido para pacientes com câncer e o primeiro que efetivamente reduziu a multiplicação do HIV.</p>

					<p class="marginBottomP">O ano de 1988 foi um marco para o tratamento no  histórico no Brasil. Com a criação do Sistema Único de Saúde e o início do fornecimento de medicamentos para o tratamento da doença. Sendo que eEm 1991 os AZT passou medicamentos antirretrovirais passaram a ser distribuídos gratuitamente.</p>
					 
					<p class="marginBottomP">Lei de 96 Em 1996 o Congresso Nacional aprovou Lei de inciativa do Senador José Sarney (9.3135ver número da Lei) que garantiu o acesso universal aos medicamentos antirretrovirais já na forma do coquetel, combinação de 3 medicamentos para melhorar a resposta ao tratamento.</p>

					<p class="marginBottomP">Com a disponibilização dos antirretrovirais tratamento, houve melhora significativa na qualidade de vida dos portadores do HIV e a mortalidade dos pacientes de Aids caiu 50% ao final no fim dos anos 90.</p>

					<p class="marginBottomP">A partir da virada do século, depois dos anos 2000, várias  foram marcados por ações que visarvam a diminuição dos custos do tratamento. A ONU fechou acordo com cinco grandes indústrias farmacêuticas para garantir a diminuição dos preços dos medicamentos usados no tratamento da Aids e o Governo Brasileiro investiu na produção nacional de medicamentos genéricos para AIDS depois da Lei dos Genéricos aprovada no Congresso Nacional em 1999,  em uma fábrica nacional de preservativos e na nacionalização de um teste que detecta o HIV em apenas 40 minutos.</p>

					<p>Atualmente, o Governo Brasileiro produz nacionalmente, em fábrica própria ou por meio de Parcerias Público-Privadas, parte dos medicamentos utilizados no tratamento. A diminuição dos custos com a compra dos medicamentos possibilita o aumento dos programas de prevenção e tratamento, beneficiando milhões de brasileiro.</p>
				</div><!-- end tab -->

				<div id="deu-positivo" class="tab-content ativo">
					<p class="marginBottomP">Para os próximos anos, a previsão é de tratamentos cada vez mais eficientes. Pesquisadores de todo o mundo estão na permanente busca por inovações tecnol[ogicastecnológicas que possam contribuir no enfrentamento da epidemia de HIV/AIDS, auxiliando na criação de medicamentos mais eficientes e de menor custo, tornando-os mais acessíveis, além de opções com cada vez menos efeitos colaterais aos pacientes. Além disto as doses fixas combinadas tem sido grande novidade para diminuir o número de pílulas tomadas e aumentar a adesão.</p>
					<p>Seguindo corretamente as recomendações médicas quanto ao tratamento, as pessoas vivendo com HIV/AIDS tem melhor qualidade de vida Os medicamentos utilizados no tratamento auxiliam no fortalecimento do sistema imunológico do paciente, aquele responsável pela defesa do corpo, e na diminuição da reprodução do vírus. A busca da carga viral indetectável tem sido um objetivo importante para melhorar as condições de vida e saúde dos portadores do HIV, impactar a mortalidade e também a transmissão do HIV. </p>
				</div>
			</div>
		</div>
	</div><!-- end wrap-content -->

</div><!-- end conteudo-interno -->

<?php include('./inc/footer.php'); ?>