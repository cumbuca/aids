<section class="linha-do-tempo">
	<h2>30 anos de luta e fatos históricos</h2>
	<div id="timeline">
		<div id="wrap-issues">
			<ul id="issues">
				<li id="1987">
					<img src="./images/timeline/1987.jpg" width="216">
					<div class="content">
						<h3>Criação do Dia Mundial da Luta Contra a Aids</h3>
						<p>A data é criada pela Assembleia Mundial da Saúde, com apoio da ONU, para reforçar a mensagem de compreensão, compaixão e tolerância com as pessoas infectadas pelo HIV.</p>
					</div>
				</li>
				<li id="1995">
					<img src="./images/timeline/1995.jpg" width="216">
					<div class="content">
						<h3>Pesquisa sobre tratamento precoce das DST</h3>
						<p>Nesse ano, cientistas descobriram que quando as DST eram tratadas precocemente, o risco de transmissão e aquisição do HIV diminuía significativamente. Com isso, o número de incidência da doença caiu drasticamente, chegando a reduzir em 42%.</p>
					</div>
				</li>
				<li id="1998">
					<img src="./images/timeline/1998.jpg" width="216">
					<div class="content">
						<h3>Lançamento da campanha “Sem Camisinha não Tem Carnaval” e “A Força da Mudança: com os jovens em campanha contra a Aids”</h3>
						<p>O ano de 1998 foi marcante em relação às campanhas para a luta contra a Aids. Duas delas foram lançadas pelo Ministério da Saúde com o intuito de incentivar o uso da camisinha e de mostrar a relevância do protagonismo do jovem nessa causa.</p>
					</div>
				</li>
				<li id="2000">
					<img src="./images/timeline/2000.jpg" width="216">
					<div class="content">
						<h3>Realização do I Fórum em HIV/Aids e DST da América Latina</h3>
						<p>Ocorrido no Rio de Janeiro, o Fórum foi de suma importância para debater os temas relacionados a Aids e ao HIV.</p>
					</div>
				</li>
				<li id="2003">
					<img src="./images/timeline/2003.jpg" width="216">
					<div class="content">
						<h3>Doação da Fundação Bill &amp; Melinda Gates</h3>
						<p>Como reconhecimento pelo bom trabalho das ONGs brasileiras que trabalhavam com portadores de HIV e Aids, a Fundação Bill &amp; Melinda Gates doou neste ano US$ 1 milhão para ações de prevenção e assistência.</p>
					</div>
				</li>
				<li id="2004">
					<img src="./images/timeline/2004.jpg" width="216">
					<div class="content">
						<h3>Mortes de Janaína Dutra e Marcela Prado</h3>
						<p>Duas lideranças transexuais e grandes colaboradoras do Programa Nacional de DST e Aids, Janaína Dutra e Marcela Prado morreram em 2004, deixando um grande legado para a luta.</p>
					</div>
				</li>
				<li id="2010">
					<img src="./images/timeline/2010-1.jpg" width="216">
					<div class="content">
						<h3>Parceria entre Brasil e África do Sul na Copa do Mundo 2010</h3>
						<p>Um ônibus com 30 mil camisinhas e folderes de prevenção contra a Aids e outras DST circulou na África do Sul na época da Copa do Mundo para promover a luta e incentivar a população a se prevenir.</p>
					</div>
				</li>
				<li id="2010">
					<img src="./images/timeline/2010-2.jpg" width="216">
					<div class="content">
						<h3>Campanha Global dos Correios Contra a Aids no Brasil</h3>
						<p>Os Correios se tornaram ponto estratégico de distribuição de material informativo sobre prevenção às DST e à Aids. Mais de 12 mil agências pelo País participaram da ação.</p>
					</div>
				</li>
				<li id="2011">
					<img src="./images/timeline/2011-1.jpg" width="216">
					<div class="content">
						<h3>Brasil anuncia produção nacional de dois novos medicamentos</h3>
						<p>Por meio de parcerias público-privadas, dois remédios importantes para o tratamento (Atazanavir e Raltegravir) começaram a ser produzidos nacionalmente.</p>
					</div>
				</li>
				<li id="2011">
					<img src="./images/timeline/2011-2.jpg" width="216">
					<div class="content">
						<h3>Ação de passagens</h3>
						<p>O governo brasileiro decidiu doar US$ 2 para a Unitaid (Central Internacional para Compra de Medicamentos) para cada passageiro que viajar de avião para o exterior.</p>
					</div>
				</li>
			</ul>
		</div>
		<ul id="dates">
			<li><a href="#1987">1987</a></li>
			<li><a href="#1995">1995</a></li>
			<li><a href="#1998">1998</a></li>
			<li><a href="#2000">2000</a></li>
			<li><a href="#2003">2003</a></li>
			<li><a href="#2004">2004</a></li>
			<li><a href="#2010">2010</a></li>
			<li><a href="#2010">2010</a></li>
			<li><a href="#2011">2011</a></li>
			<li><a href="#2011">2011</a></li>
		</ul>
		<a href="#" id="next">+</a>
		<a href="#" id="prev">-</a>
	</div>
</section>