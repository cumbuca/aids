<?php include('./inc/header-interna.php'); ?>

<?php include('./inc/timeline.php'); ?>

<div class="conteudo-interno">
	<?php include('./inc/breadcrumb.php'); ?>
	<div class="topo-interna">
		<img src="./images/topo-interna-porque-e-como-fazer-o-teste.png" alt="">
		<h2>Porque e como<br>fazer o teste</h2>
	</div>
	<div class="wrap-content porque-fazer">
		<p class="marginBottomP txt-normal">
				Se você transou sem camisinha, teve contato direto com o sêmen ou sangue 
				de outra pessoa, compartilhou seringas, agulhas, é melhor fazer o teste de HIV. 
				De qualquer maneira, qualquer pessoa que é sexualmente ativa nos últimos 10 
				anos deve fazer o teste de HIV. </p>

				<p class="marginBottomP txt-normal">
				Os testes são feitos gratuitamente nas unidades de saúde do Sistema Único de 
				Saúde (SUS). Acesse o site do Departamento de DST, Aids e Hepatites Virais 
				do Ministério da Saúde e encontre a unidade de saúde mais próxima de você: 
				www.Aids.gov.br/.</p>

				<p class="marginBottomP txt-normal">
				É simples e o resultado pode sair em até 30 minutos.
				</p>
				  
				<p class="marginBottomP txt-normal">
				E aí, #PartiuTeste?<br>
				Lembre-se: o teste deve ser feito pelo menos 30 dias após a exposição a 
				situação de risco, para um resultado mais confiável. Esse é o período da 
				chamada Janela Imunológica. Antes desse período é mais difícil de detectar os 
				anticorpos do vírus, podendo gerar um falso resultado negativo.
					</p>

					<p class="marginBottomP txt-normal">
						Lembre-se: o teste deve ser feito pelo menos 30 dias
						após a exposição a situação de risco, para um resultado
						mais confiável. Esse é o período da chamada Janela Imunológica.
						Antes desse período é mais difícil de detectar os anticorpos do vírus,
						podendo gerar um falso resultado negativo.
					</p>
	</div> <!-- end wrpa-content -->
</div>

<?php include('./inc/footer.php'); ?>