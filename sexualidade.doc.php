<?php include('./inc/header-interna.php'); ?>

<?php include('./inc/timeline.php'); ?>

<div class="conteudo-interno">
	<?php include('./inc/breadcrumb.php'); ?>
	<div class="topo-interna">
		<img src="./images/topo-interna-sexualidade.png" alt="">
		<h2 class="yellowTit">Sexualidade.doc</h2>
	</div>


		<div class="wrap-content tratamento">
		<div class="wrap-abas">
			<ul class="tabs tabVertical">							
				<li>
					<a id="direitos" class="tab txt-normal" href="javascript:void(0);">Filmes</a>
				</li>
				<li>
					<a id="deu-positivo" class="tab txt-normal" href="javascript:void(0);">Documentários</a>
				</li>				
				<li>
					<a id="cuidados" class="tab txt-normal" href="javascript:void(0);">Livros</a>
				</li>
				<li>
					<a id="sus" class="tab txt-normal ativo" href="javascript:void(0);">Artigos e pesquisas</a>
				</li>
			</ul>
			<div class="tabs-content-container verticalTabContainer">
							
				<div id="direitos" class="tab-content ativo">
					
					<ul id="listaFilmes" class="listaFilmeScroll">
						<li>
							<div class="wrap-coverFilme">
								<img src="./images/cover-philadelphia.jpg" alt="">
							</div>
							<h5 class="titFilme">
							 Philadelphia (Filadélfia) – 1993
							 </h5>
							<p class="txt-descricao">								
								Diretor: Jonathan Demme<br><br>
								Um promissor advogado do escritório Filadélfia é despedido após
								descobrirem que havia sido diagnosticado com AIDS. Ele contrata
								o serviço de um advogado negro que é forçado a encarar seus próprios
								medos e preconceitos. 
 							</p> 							

						</li>

						<li>
							<div class="wrap-coverFilme">
								<img src="./images/cover-thecure.jpg" alt="">
							</div>
							<h5 class="titFilme">
							 The Cure (A Cura) – 1995
							 </h5>
							<p class="txt-descricao">								
								Diretor: Peter Horton<br><br>
								Erik é um garoto solitário que se torna amigo do seu vizinho,
								Dexter, que tem 11 anos e é portador do vírus HIV. Quando os
								dois garotos leem que um médico descobriu a cura da AIDS,
								os meninos tentam chegar a este médico de Nova Orleans.
 							</p> 							

						</li>

						<li>
							<div class="wrap-coverFilme">
								<img src="./images/cover-yesterday.jpg" alt="">
							</div>
							<h5 class="titFilme">
							 Yesterday (Yesterday) – 2004
							 </h5>
							<p class="txt-descricao">								
								Diretor: Darrell Roodt<br><br>
								Yesterday é uma moça do sul da África que adoece e descobre ser
								 portadora do vírus HIV. Mesmo rejeitada por seu marido, ela luta
								  para sobreviver e ver sua filha indo para a escola.
 							</p> 							

						</li>


						<li>
							<div class="wrap-coverFilme">
								<img src="./images/cover-leo.jpg" alt="">
							</div>
							<h5 class="titFilme">
							Tout Contre Léo (Todos Contra Leo) – 2002
							 </h5>
							<p class="txt-descricao">								
								Diretor: Christophe Honoré<br><br>
								Marcel é um menino de 12 anos que descobre, após escutar atrás 
								da porta uma conversa de família, que seu irmão mais velho, Leo,
								 é soropositivo. Todos da família aceitam Leo, exceto Marcel.
								  A trama se desenrola através dos olhos do garoto.
 							</p> 							

						</li>


						<li>
							<div class="wrap-coverFilme">
								<img src="./images/cover-jeffrey.jpg" alt="">
							</div>
							<h5 class="titFilme">
							Jeffrey (Jeffrey – De Caso com a Vida) – 1995
							 </h5>
							<p class="txt-descricao">								
								Diretor: Christopher Ashley<br><br>
								Esta comédia sobre a AIDS conta a história de Jeffrey,
								 um homem gay que decide ser celibatário em face da epidemia de AIDS.
								  Após tomar essa decisão, ele conhece seu grande amor.
 							</p> 							

						</li>

						<li>
							<div class="wrap-coverFilme">
								<img src="./images/cover-normalhear.jpg" alt="">
							</div>
							<h5 class="titFilme">
							The Normal Heart – 2014
							 </h5>
							<p class="txt-descricao">								
								Diretor: Ryan Murphy<br><br>
								Baseado em uma peça teatral, conta a história do início da epidemia
								 da Aids nos EUA, quando a doença ainda não era conhecida e ainda
								  era chamada de “câncer gay”. A angústia da busca por conhecimento
								   sobre a doença e um tratamento é retratada de forma chocante.
 							</p> 							

						</li>

						<li>
							<div class="wrap-coverFilme">
								<img src="./images/cover-dallas.jpg" alt="">
							</div>
							<h5 class="titFilme">
							Clube de Compra Dallas – 2014
							 </h5>
							<p class="txt-descricao">								
								Diretor: Jean-Marc Vallée<br><br>
								Baseado em fatos reais, o filme se passa na década de 80,
								 quando um cowboy texano é diagnosticado com o vírus HIV.
								  Ele procura formas ilegais de adquirir medicamentos não 
								  autorizados para o tratamento e acaba criando um novo negócio.
 							</p> 							

						</li>

						<li>
							<div class="wrap-coverFilme">
								<img src="./images/cover-vida-continua.jpg" alt="">
							</div>
							<h5 class="titFilme">
							E a vida continua
							 </h5>
							<p class="txt-descricao">								
								Diretor: Roger Spottiswoode<br><br>
								E a Vida Continua é um filme para a televisão de drama americano
								 lançado em 1993 dirigido por Roger Spottiswoode. O roteiro de Arnold Schulman
								  é baseado no livro de 1987 de não-ficção mais vendido And the Band Played On:
								   Politics, People, and the AIDS Epidemic de Randy Shilts.
								    O filme estreou no Festival Internacional de Cinema de Montreal 
								    antes de ser transmitido na HBO em 11 de setembro de 1993. Mais tarde, 
								    foi lançado no Reino Unido, Canadá, Espanha, Alemanha, Argentina, Áustria, 
								    Itália, Suécia, Países Baixos, França, Dinamarca, Nova Zelândia e Austrália.
 							</p> 							

						</li>

						<li>
							<div class="wrap-coverFilme">
								<img src="./images/cover-cazuza.jpg" alt="">
							</div>
							<h5 class="titFilme">
							Cazuza – O Tempo Não Para – 2003
							 </h5>
							<p class="txt-descricao">								
								Diretor: Sandra Werneck e Walter Carvalho<br><br>
								A história real do cantor Cazuza, retrata de forma emocionante como a Aids
								 o levou do topo da carreira à sua morte, quando ainda não existia
								  o tratamento oferecido hoje.
 							</p> 							

						</li>

						<li>
							<div class="wrap-coverFilme">
								<img src="./images/cover-boasorte.jpg" alt="">
							</div>
							<h5 class="titFilme">
							Boa Sorte 
							 </h5>
							<p class="txt-descricao">								
								Diretor: Carolina Jabor<br><br>
								O adolescente João (João Pedro Zappa) tem uma série de problemas comportamentais:
								 ele é ignorado pelos pais e se torna agressivo com os amigos de escola. 
								 Quando é diagnosticado com depressão, seus familiares decidem interná-lo em uma 
								 clínica psiquiátrica. No local, ele conhece Judite (Deborah Secco),
								  paciente HIV positivo e dependente química, em fase terminal. 
								  Apesar do ambiente hostil, os dois se apaixonam e iniciam um romance.
								   Mas Judite tem medo que a sua morte abale a saúde de João.
 							</p> 							

						</li>


						<li>
							<div class="wrap-coverFilme">
								<img src="./images/cover-angels.jpg" alt="">
							</div>
							<h5 class="titFilme">
							Anjos da América
							 </h5>
							<p class="txt-descricao">								
								Diretor: Mike Nochols<br><br>
								Em 1985, Prior Walter conta para Louis Ironson, que foi seu amante por 4 anos
								 e que está com AIDS. Louis se afasta, deixando o namorado doente e solitário,
								  fazendo-o se sentir culpado. Joe Pitt, um advogado mórmon, tem a chance de trabalhar
								   em Washington e, para isto, basta que um influente advogado, Roy Cohn, faça uma ligação. 
								   Joe sente vontade de aceitar o emprego, mas antes precisa falar com Harper, sua esposa,
								    que passa os dias tomando Valium e aprendendo sexo na teoria. No meio disto,
								     um anjo convida Prior para ser o profeta da morte.
 							</p> 							

						</li>


					</ul>
					
				</div>
				<div id="deu-positivo" class="tab-content">
					<ul id="listaFilmes" class="listaDocsScroll">
							<li>
								<div class="wrap-coverFilme">
									<img src="./images/cover-praga.jpg" alt="">
								</div>
								<h5 class="titFilme">
								How  to Survive a Plague (Como Sobreviver a uma Praga) - 2012
								 </h5>
								<p class="txt-descricao">								
								Diretor: David France<br><br>
								Um grupo de jovens ativistas resiste às opressões e reverte a maré de uma epidemia,
								 não deixando a AIDS se tornar uma sentença de morte. Eles salvam suas próprias vidas
								  e a de outras pessoas, esvaziando as enfermarias dos hospitais norte-americanos.
								</p> 							

							</li>


							<li>
								<div class="wrap-coverFilme">
									<img src="./images/cover-were-here.jpg" alt="">
								</div>
								<h5 class="titFilme">
								We Were Here (Estávamos Aqui) – 2011
								 </h5>
								<p class="txt-descricao">								
								Diretor: David Weissman<br><br>
								O filme retrata as consequências da chegada da AIDS e como São Francisco (EUA)
								 se tornou um exemplo de combate à doença a partir da história de cinco pessoas no início dos anos 1970.
								</p> 							

							</li>


								<li>
									<div class="wrap-coverFilme">
										<img src="./images/cover-positivas.jpg" alt="">
									</div>
									<h5 class="titFilme">
									Positivas – 2009
									 </h5>
									<p class="txt-descricao">								
									Diretor: Susanna Lira<br><br>
									O documentário acompanha a vida de Cida, Heli, Rosária, Medianeira, 
									Sílvia, Ana Paula e Michelle, mulheres que foram surpreendidas pela 
									notícia da doença em um ambiente até então seguro e moralmente “adequado”.
									</p> 							

								</li>


								<li>
									<div class="wrap-coverFilme">
										<img src="./images/cover-blue.jpg" alt="">
									</div>
									<h5 class="titFilme">
									Blue (Blue) – 1993
									 </h5>
									<p class="txt-descricao">								
									Diretor: Derek Jarman<br><br>
									Filme póstumo do poeta, pintor e cineasta Derek Jarman,
									 filmado quando sua saúde estava bastante debilitada pela AIDS.
									  Inspirado nas ideias de Yves Klein e em reflexões pessoais sobre arte,
									   poesia, memória, tempo e morte.
									</p> 							

								</li>

								<li>
									<div class="wrap-coverFilme">
										<img src="./images/cover-beija-flor.jpg" alt="">
									</div>
									<h5 class="titFilme">
									Codinome Beija-flor – 2012
									 </h5>
									<p class="txt-descricao">								
									Diretor: Higor Rodrigues<br><br>
									O filme aborda de forma sensível o universo dos soropositivos,
									 narrando suas descobertas, o enfrentamento e o preconceito.
									</p> 							

								</li>
					</ul>	

				</div>
				<div id="cuidados" class="tab-content">
					Sem conteúdo..

				</div><!-- end tab -->
				
				<div id="sus" class="tab-content">
					
					<ul id="listaFilmes" class="pesquisasArtigos">																	
						<li class="marginBottom">							
							<h5 class="titFilme">
							 AIDS e infecção pelo HIV no Brasil: uma epidemia multifacetada
							 </h5>
							<p class="txt-descricao">								
								Autores: Ana Maria de Brito, Euclides Ayres de Castilho e Célia Landmann 
								Szwarcwald.Artigo que fala sobre a epidemia da infecção pelo HIV e da AIDS como 
								fenômeno global, dinâmico e instável, e as mudanças no perfil da doença no 
								Brasil de acordo com a difusão geográfica. Disponível em:<br> www.scielo.br/pdf/rsbmt/v34n2/a10v34n2)	
 							</p> 							

						</li>

						<li class="marginBottom">							
							<h5 class="titFilme">
							 Adesão ao tratamento, acesso e qualidade da assistência em AIDS no Brasil
							 </h5>
							<p class="txt-descricao">								
								Autores: Maria Inês Batistella Nemes, Elen Rose Lodeiro Castanheira, Ernani
								Tiaraju de Santa Helena, Regina Melchior, Joselita Magalhães Caraciolo, 
								Cáritas Relva Basso, Maria Teresa Seabra Soares de Britto e Alves, Tatianna 
								Meireles Dantas de Alencar e Dulce Aurélia de Souza Ferraz.
								O artigo discute as relações existentes entre adesão, qualidade e acesso aos 
								serviços de saúde para o sucesso da terapia antirretroviral de alta potência 
								para a AIDS.Disponível em:<br> www.scielo.br/scielo.php?script=sci_arttext&pid=S0104-

42302009000200028&lng=pt&nrm=isso
 							</p> 							

						</li>


						<li>							
							<h5 class="titFilme">
							 Homossexualidade e saúde: desafios para a terceira década de epidemia de HIV/AIDS
							 </h5>
							<p class="txt-descricao">								
								Autor: Veriano Terto Jr<br>
								O artigo aponta os desafios enfrentados na promoção da saúde dos 
								homossexuais portadores do HIV, apesar dos mesmos se destacarem na 
								defesa dos direitos, enfrentando a epidemia e mobilizando ONGs.
								Disponível em: <br> www.scielo.br/scielo.php?pid=S0104-71832002000100008&script=sci_arttext
 							</p> 							

						</li>
					</ul>

					
					
				</div>
			</div>
		</div>
	</div><!-- end wrap-content -->



</div><!-- end conteudo-interno -->

<?php include('./inc/footer.php'); ?>