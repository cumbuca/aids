<?php include('./inc/header-interna.php'); ?>

<?php include('./inc/timeline.php'); ?>

<div class="conteudo-interno">
	<?php include('./inc/breadcrumb.php'); ?>
	<div class="topo-interna">
		<img src="./images/topo-interna-protocolo-de-atendimento.png" alt="">
		<h2>novo protocolo<br>de atendimento</h2>
	</div>
	<div class="wrap-content protocolo-atendimento">
		<div class="wrap-abas">
			<ul class="tabs">
				<!-- <li>
					<a id="prevencao" class="tab" href="javascript:void(0);">prevenção</a>
				</li>
				<li>
					<a id="teste" class="tab" href="javascript:void(0);">teste</a>
				</li> -->
				<li>
					<a id="tratamento" class="tab ativo" href="javascript:void(0);">tratamento</a>
				</li>
			</ul>
			<div class="tabs-content-container">
				<div id="prevencao" class="tab-content">
					<img src="./images/icon-prevencao.png" alt="">
					<p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para.Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para.</p>
				</div>
				<div id="teste" class="tab-content">
					<img src="./images/icon-teste.png" alt="">
					<p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para.Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para.</p>
				</div>
				<div id="tratamento" class="tab-content ativo">
					<img src="./images/icon-tratamento.png" alt="">
					<p class="marginBottomP">
						A Organização das Nações Unidas (ONU) reconhece o Brasil como referência 
						no enfrentamento à epidemia deAids e no tratamento de pessoas portadoras do vírus HIV. 
					</p>	 
					<p class="marginBottomP">
					O protocolo seguido pelo Sistema Único de Saúde (SUS) é pioneiro na oferta 
					de tratamento independente do estágio da doença. O documento foi construído 
					por um Comitê de Consenso Terapêutico, submetido a uma consulta pública  e 
					foi  publicado em portaria Ministerial em 29/11/2013. 
					</p>
					<p class="marginBottomP">
					Voltado para a classe médica e pesquisadores sobre HIV e Aids, o documento 
					agora está disponível para download por toda a população.
					Acesse aqui o “Protocolo Clínico e Diretrizes Terapêuticas para Manejo da 
					infecção pelo HIV em Adultos”: <LINK PARA DOWNLOAD>.
					Além disto, versões para todos os tipos de smartphones estão disponíveis nas 
					lojas da Apple, Android e Amazon, inteiramente grátis.
					</p>					
				</div>
			</div>
		</div>
	</div>
</div>

<?php include('./inc/footer.php'); ?>