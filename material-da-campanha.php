<?php include('./inc/header-interna.php'); ?>

<?php include('./inc/timeline.php'); ?>

<div class="conteudo-interno">
	<?php include('./inc/breadcrumb.php'); ?>
	<div class="topo-interna">
		<img src="./images/topo-interna-material-da-campanha.png" alt="">
		<h2>material da<br>camapanha</h2>
	</div>
	<div class="wrap-content material-da-campanha">
		<p>Ajude quem você ama. Compartilhe a campanha com seus amigos e nos ajude a conscientizá-los. Se previna. Se conheça.<br>E aí, #PartiuTeste?</p>
		
		<ul>
			<li>
				<h3>vídeo</h3>
				<img src="./images/video.jpg" alt="">
				<a href="./video/aids.mp4" target="_blank" class="link-download">download</a>
			</li>
			<li>
				<h3>pdf</h3>
				<img src="./images/pdf1.jpg" alt="">
				<a href="./pdf/pdf1.pdf" target="_blank" class="link-download">download</a>
			</li>
			<li>
				<h3>pdf</h3>
				<img src="./images/pdf2.jpg" alt="">
				<a href="./pdf/pdf2.pdf" target="_blank" class="link-download">download</a>
			</li>
		</ul>
	</div>
</div>

<?php include('./inc/footer.php'); ?>