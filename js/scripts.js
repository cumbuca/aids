$(document).ready(function(){

	// ABAS DO APP

    $('.tabs li a').on('click', function()  {

        var currentAttrValue = $(this).attr('id');

 		$('.tab').removeClass('ativo');
        $("#" + currentAttrValue).addClass('ativo');
 
        $('.tabs-content-container .tab-content').removeClass('ativo');
        $('.tabs-content-container #'+currentAttrValue ).addClass('ativo');

    });

    // LIGHTBOX DO VÍDEO DA CAMPANHA

  //   $('.link-filme').on('click', function(){
		// $(".wrap-video").show();
		// $(".content-video .close-btn").click(function(){
		// 	$(".wrap-video").hide();
		// });
  //   });

    

    //Slim scroll / páginas internas   
    $('.listaFilmeScroll').slimScroll({
        width: '348px',
        height: '610px',
        railVisible: true,
        alwaysVisible: true     
    });


   //Slim scroll / páginas internas   
    $('.listaDocsScroll').slimScroll({
        width: '348px',
        height: '610px',
        railVisible: true,
        alwaysVisible: true     
    });




    // TIMELINE DO APP
    
    $('#timeline').timelinr({
        arrowKeys: 'true'
    });

});

window.onload = function(){
    $('#container .linha-do-tempo #issues li').hover(function(){
        if( $(this).hasClass('selected') ){
            $(this).find('.content').stop().fadeIn();
        }
    }, function(){
        $('#container .linha-do-tempo #issues li .content').stop().fadeOut();
    });
}